package com.yiluhub.airflux.entities;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class AirportRepositoryTest {
    private AirportRepository repository;

    @Before
    public void setup() {
        repository = new AirportRepository();
    }

    @Test
    public void returnsNullWhenFindWithAnIdThatDoesNotExist() {
        assertNull(repository.findById("foo"));
    }

    @Test
    public void findTheAiportWithAValidId() {
        Airport airport = repository.findById("TXL");
        assertEquals("TXL", airport.getId());
        assertEquals("Berlin", airport.getCity());
    }

    @Test
    public void returnsNullWhenFindByCityWithInvalidCity() {
        assertNull(repository.findByCity("Paris"));
    }

    @Test
    public void findTheAiportWithAValidCity() {
        Airport airport = repository.findByCity("Berlin");
        assertEquals("TXL", airport.getId());
        assertEquals("Berlin", airport.getCity());
    }
}

package com.yiluhub.airflux.entities;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class FlightScheduleRepositoryTest {
    private FlightScheduleRepository repository;

    @Before
    public void setup() {
        repository = new FlightScheduleRepository();
    }

    @Test
    public void returnsEmptyListWhenFilteringByInvalidOrigin() {
        assertTrue(repository.getScheduleByOrigin("CDG").isEmpty());
    }

    @Test
    public void returnsAllFlightScheduleForTheGivenOrigin() {
        List<FlightSchedule> schedules = repository.getScheduleByOrigin("MUC");
        assertEquals(8, schedules.size());
    }
}

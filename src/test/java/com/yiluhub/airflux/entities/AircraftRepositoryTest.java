package com.yiluhub.airflux.entities;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class AircraftRepositoryTest {

    private AircraftRepository repository;

    @Before
    public void setup() {
        repository = new AircraftRepository();
    }

    @Test
    public void returnsNullWhenTheAiportDoesNotExist() {
        Airport airport = new Airport("foo", "Bar");
        assertNull(repository.aircraftAt(airport));
    }

    @Test
    public void returnsTheMatchingAircraftForTheAiport() {
        Airport airport = new Airport("TXL", "Berlin");
        Aircraft aircraft = repository.aircraftAt(airport);
        assertEquals("Boeing 737 - Berlin", aircraft.getName());
        assertEquals("Boeing 737", aircraft.getId());
        assertEquals("FL-0001", aircraft.getRegistration());
    }

    @Test
    public void returnsNullIfNoAircraftExistsWithTheGivenRegistration() {
        assertNull(repository.findAircraftByRegistration("FO-123"));
    }

    @Test
    public void returnsTheFoundAircraftByRegistration() {
        Aircraft aircraft = repository.findAircraftByRegistration("FL-0004");
        assertEquals("Airbus A320 - Hamburg", aircraft.getName());
        assertEquals("FL-0004", aircraft.getRegistration());
        assertEquals("Hamburg", aircraft.getCity());
        assertEquals("Airbus A320", aircraft.getId());
    }
}

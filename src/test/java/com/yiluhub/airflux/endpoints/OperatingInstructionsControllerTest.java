package com.yiluhub.airflux.endpoints;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

public class OperatingInstructionsControllerTest {
    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(new OperatingInstructionsController()).build();
    }

    @Test
    public void returnErrorWhenRegistrationIsMissin() throws Exception {
        MockHttpServletResponse response = this.mockMvc
                .perform(get("/operationsplan").accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andReturn()
                .getResponse();

        assertEquals(response.getStatus(), HttpStatus.BAD_REQUEST.value());
    }

    @Test
    public void returnEmptySetWithInvalidRegistration() throws Exception {
        MockHttpServletResponse response = this.mockMvc
                .perform(get("/operationsplan").param("registration", "foo").accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andReturn()
                .getResponse();

        assertEquals(response.getStatus(), HttpStatus.OK.value());
        JSONArray content = new JSONArray(response.getContentAsString());
        assertEquals(0, content.length());
    }

    @Test
    public void returnTheOperatingInstructinsForTheGivenRegistration() throws Exception {
        MockHttpServletResponse response = this.mockMvc
                .perform(get("/operationsplan").param("registration", "FL-0003").accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andReturn()
                .getResponse();

        assertEquals(response.getStatus(), HttpStatus.OK.value());
        JSONArray content = new JSONArray(response.getContentAsString());
        assertEquals(4, content.length());
        JSONObject object = content.getJSONObject(0);
        assertEquals("LHR", object.get("origin"));
        assertEquals("HAM", object.get("destination"));
        assertEquals("2018-04-13T09:00:00.000-0300", object.get("departure"));
    }
}

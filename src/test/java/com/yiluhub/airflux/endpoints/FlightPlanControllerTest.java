package com.yiluhub.airflux.endpoints;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

public class FlightPlanControllerTest {
    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(new FlightPlanControlller()).build();
    }

    @Test
    public void returnsAllTheFlightPlans() throws Exception {
        MockHttpServletResponse response = this.mockMvc
                .perform(get("/flightplan").accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andReturn()
                .getResponse();

        assertEquals(response.getStatus(), HttpStatus.OK.value());
        JSONArray content = new JSONArray(response.getContentAsString());
        JSONObject flight = content.getJSONObject(0);
        assertEquals("TXL", flight.get("origin"));
        assertEquals("MUC", flight.get("destination"));
        assertEquals("2018-04-13T10:00:00.000-0300", flight.get("departure"));
        assertEquals("2018-04-13T11:00:00.000-0300", flight.get("arrival"));
        assertEquals("Boeing 737", flight.get("equipament"));
    }

    @Test
    public void returnsAllTheFlightPlansFilteredByAirport() throws Exception {
        MockHttpServletResponse response = this.mockMvc
                .perform(get("/flightplan").param("airport", "LHR").accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andReturn()
                .getResponse();

        assertEquals(response.getStatus(), HttpStatus.OK.value());
        JSONArray content = new JSONArray(response.getContentAsString());
        assertEquals(4, content.length());
        JSONObject flight = (JSONObject) content.get(0);
        assertEquals("LHR", flight.get("origin"));
        assertEquals("HAM", flight.get("destination"));
        assertEquals("2018-04-13T09:00:00.000-0300", flight.get("departure"));
        assertEquals("2018-04-13T11:30:00.000-0300", flight.get("arrival"));
        assertEquals("Boeing 747-400", flight.get("equipament"));
    }

    @Test
    public void returnEmptyListOfFlightPlansWhenInvalidAirportIsGiven() throws Exception {
        MockHttpServletResponse response = this.mockMvc
                .perform(get("/flightplan").param("airport", "CWB").accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andReturn()
                .getResponse();

        assertEquals(response.getStatus(), HttpStatus.OK.value());
        JSONArray content = new JSONArray(response.getContentAsString());
        assertEquals(0, content.length());
    }
}

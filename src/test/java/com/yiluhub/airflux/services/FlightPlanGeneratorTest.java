package com.yiluhub.airflux.services;

import com.yiluhub.airflux.entities.Flight;
import org.junit.Test;

import java.time.ZonedDateTime;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class FlightPlanGeneratorTest {
    @Test
    public void combinesTheFlightScheduleAndAircraftsToGenerateFlightPlan() {
        FlightPlanGenerator generator = new FlightPlanGenerator();
        List<Flight> flights = generator.getAllFlights();

        Flight flight = flights.get(0);
        assertEquals("TXL", flight.getOrigin());
        assertEquals("MUC", flight.getDestination());
        assertEquals(ZonedDateTime.parse("2018-04-13T10:00:00.000-03:00"), flight.getDeparture());
        assertEquals(ZonedDateTime.parse("2018-04-13T11:00:00.000-03:00"), flight.getArrival());
        assertEquals("Boeing 737", flight.getEquipament());

        flight = flights.get(4);
        assertEquals("TXL", flight.getOrigin());
        assertEquals("HAM", flight.getDestination());
        assertEquals(ZonedDateTime.parse("2018-04-13T21:00:00.000-03:00"), flight.getDeparture());
        assertEquals(ZonedDateTime.parse("2018-04-13T21:40:00.000-03:00"), flight.getArrival());
        assertEquals("Boeing 737", flight.getEquipament());
    }
}

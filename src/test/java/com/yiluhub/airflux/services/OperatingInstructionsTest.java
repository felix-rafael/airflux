package com.yiluhub.airflux.services;

import com.yiluhub.airflux.entities.OperatingInstruction;
import org.junit.Before;
import org.junit.Test;

import java.time.ZonedDateTime;
import java.util.List;

import static org.junit.Assert.*;

public class OperatingInstructionsTest {
    private OperatingInstructions operatingInstructions;

    @Before
    public void setup() {
        operatingInstructions = new OperatingInstructions();
    }

    @Test
    public void returnsEmptyWithInvalidRegistration() {
        assertTrue(operatingInstructions.generateOperatingInstructions("foo").isEmpty());
    }

    @Test
    public void returnsAllOperatingInstructionsForTheGivenRegistration() {
        List<OperatingInstruction> instructions = operatingInstructions.generateOperatingInstructions("FL-0002");
        assertEquals(8, instructions.size());
        OperatingInstruction instruction = instructions.get(0);
        assertEquals("MUC", instruction.getOrigin());
        assertEquals("LHR", instruction.getDestination());
        assertEquals(ZonedDateTime.parse("2018-04-13T10:00:00.000-03:00"), instruction.getDeparture());
    }
}

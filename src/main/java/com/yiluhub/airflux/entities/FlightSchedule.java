package com.yiluhub.airflux.entities;

import java.time.Duration;
import java.time.LocalTime;

public class FlightSchedule {
    private LocalTime timeOfDeparture;
    private String origin;
    private String destination;
    private LocalTime flightTime;

    public FlightSchedule() {
    }

    public FlightSchedule(LocalTime timeOfDeparture, String origin, String destination, LocalTime flightTime) {
        this.timeOfDeparture = timeOfDeparture;
        this.origin = origin;
        this.destination = destination;
        this.flightTime = flightTime;
    }

    public LocalTime getTimeOfDeparture() {
        return timeOfDeparture;
    }

    public void setTimeOfDeparture(LocalTime timeOfDeparture) {
        this.timeOfDeparture = timeOfDeparture;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public LocalTime getFlightTime() {
        return flightTime;
    }

    public void setFlightTime(LocalTime flightTime) {
        this.flightTime = flightTime;
    }
}

package com.yiluhub.airflux.entities;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class AircraftRepository {
    /**
     * Return all aircrafts
     * @return List contaning all aircrafts
     */
    public List<Aircraft> getAll() {
        List<Aircraft> aircrafts = new ArrayList<>();
        aircrafts.add(new Aircraft("Boeing 737 - Berlin", "registration FL-0001"));
        aircrafts.add(new Aircraft("Airbus A321 - Munich", "registration FL-0002"));
        aircrafts.add(new Aircraft("Boeing 747-400 - London", "registration FL-0003"));
        aircrafts.add(new Aircraft("Airbus A320 - Hamburg", "registration FL-0004"));

        return aircrafts;
    }

    /**
     * Find an aircraft at the given airport id
     * @param airportId airport identification, e.g. TXL
     * @return the Aircraft found null otherwise
     */
    public Aircraft aircraftAt(Airport airport) {
        if (airport == null) {
            return null;
        }

        List<Aircraft> aircrafts = getAll().stream().filter(a -> a.getName().contains(airport.getCity())).collect(Collectors.toList());
        if (aircrafts.isEmpty()) {
            return null;
        }

        return aircrafts.get(0);
    }

    /**
     * Find an aircraft for the given registration
     * @param registration identification of the aircraft
     * @return the aircraft found, null otherwise
     */
    public Aircraft findAircraftByRegistration(String registration) {
        List<Aircraft> aircrafts = getAll().stream().filter(p -> p.getRegistration().equals(registration)).collect(Collectors.toList());
        if (aircrafts.isEmpty()) {
            return null;
        }

        return aircrafts.get(0);
    }
}

package com.yiluhub.airflux.entities;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class AirportRepository {
    /**
     * Get all available airports
     * @return List of all airports
     */
    public List<Airport> getAll() {
        List<Airport> airports = new ArrayList<>();
        airports.add(new Airport("TXL", "Berlin"));
        airports.add(new Airport("MUC", "Munich"));
        airports.add(new Airport("LHR", "London"));
        airports.add(new Airport("HAM", "Hamburg"));

        return airports;
    }

    /**
     * Find an specific airport with the given id
     * @param id unique identifier of the airport. e.g. TXL
     * @return The airport found null otherwise
     */
    public Airport findById(String id) {
        List<Airport> airports = getAll().stream().filter(a -> a.getId().equals(id)).collect(Collectors.toList());
        if (airports.isEmpty()) {
            return null;
        }

        return airports.get(0);
    }

    /**
     * Find an specific airport in a city
     * @param city the city name
     * @return the airport found, null otherwise
     */
    public Airport findByCity(String city) {
        List<Airport> airports = getAll().stream().filter(a -> a.getCity().equals(city)).collect(Collectors.toList());
        if (airports.isEmpty()) {
            return null;
        }

        return airports.get(0);
    }
}

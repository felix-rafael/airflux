package com.yiluhub.airflux.entities;

public class Airport {
    private String id;
    private String city;

    public Airport() {
    }

    public Airport(String id, String city) {
        this.id = id;
        this.city = city;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}

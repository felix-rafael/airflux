package com.yiluhub.airflux.entities;

import java.util.Arrays;

public class Aircraft {
    private String name;
    private String registration;

    public Aircraft() {
    }

    public Aircraft(String name, String registration) {
        this.name = name;
        this.registration = registration;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegistration() {
        return registration.replace("registration ", "");
    }

    public void setRegistration(String registration) {
        this.registration = registration;
    }

    public String getCity() {
        String[] parts = name.split("-");
        return parts[parts.length - 1].trim();
    }

    public String getId() {
        String[] parts = name.split("-");
        String[] equipamentItems = Arrays.copyOf(parts, parts.length - 1);

        return String.join("-", equipamentItems).trim();
    }
}

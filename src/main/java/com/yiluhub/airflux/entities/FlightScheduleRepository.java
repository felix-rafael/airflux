package com.yiluhub.airflux.entities;

import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FlightScheduleRepository {
    public List<FlightSchedule> getAll() {
        List<FlightSchedule> schedules = new ArrayList<>();

        // TXL
        schedules.add(new FlightSchedule(LocalTime.parse("10:00"), "TXL", "MUC", LocalTime.parse("01:00")));
        schedules.add(new FlightSchedule(LocalTime.parse("15:00"), "TXL", "MUC", LocalTime.parse("01:00")));
        schedules.add(new FlightSchedule(LocalTime.parse("16:00"), "TXL", "MUC", LocalTime.parse("01:00")));
        schedules.add(new FlightSchedule(LocalTime.parse("18:00"), "TXL", "MUC", LocalTime.parse("01:00")));
        schedules.add(new FlightSchedule(LocalTime.parse("21:00"), "TXL", "HAM", LocalTime.parse("00:40")));

        //MUC
        schedules.add(new FlightSchedule(LocalTime.parse("10:00"), "MUC", "LHR", LocalTime.parse("02:00")));
        schedules.add(new FlightSchedule(LocalTime.parse("13:00"), "MUC", "TXL", LocalTime.parse("01:00")));
        schedules.add(new FlightSchedule(LocalTime.parse("15:00"), "MUC", "TXL", LocalTime.parse("01:00")));
        schedules.add(new FlightSchedule(LocalTime.parse("15:30"), "MUC", "LHR", LocalTime.parse("02:00")));
        schedules.add(new FlightSchedule(LocalTime.parse("17:30"), "MUC", "HAM", LocalTime.parse("01:00")));
        schedules.add(new FlightSchedule(LocalTime.parse("18:00"), "MUC", "LHR", LocalTime.parse("02:30")));
        schedules.add(new FlightSchedule(LocalTime.parse("20:00"), "MUC", "LHR", LocalTime.parse("02:00")));
        schedules.add(new FlightSchedule(LocalTime.parse("22:00"), "MUC", "TXL", LocalTime.parse("01:00")));

        // LHR
        schedules.add(new FlightSchedule(LocalTime.parse("09:00"), "LHR", "HAM", LocalTime.parse("02:30")));
        schedules.add(new FlightSchedule(LocalTime.parse("12:00"), "LHR", "TXL", LocalTime.parse("02:00")));
        schedules.add(new FlightSchedule(LocalTime.parse("17:00"), "LHR", "TXL", LocalTime.parse("02:00")));
        schedules.add(new FlightSchedule(LocalTime.parse("20:30"), "LHR", "MUC", LocalTime.parse("02:00")));

        // HAM
        schedules.add(new FlightSchedule(LocalTime.parse("10:00"), "HAM", "MUC", LocalTime.parse("01:00")));
        schedules.add(new FlightSchedule(LocalTime.parse("13:00"), "HAM", "MUC", LocalTime.parse("01:00")));
        schedules.add(new FlightSchedule(LocalTime.parse("20:00"), "HAM", "MUC", LocalTime.parse("01:00")));

        return schedules;
    }

    /**
     * Return all the schedules by the given origin airport
     * @see FlightSchedule
     * @param origin airport identification e.g. TXL
     * @return List of FlightSchedule for the given airport
     */
    public List<FlightSchedule> getScheduleByOrigin(String origin) {
        List<FlightSchedule> schedules = getAll().stream().filter(s -> s.getOrigin().equals(origin)).collect(Collectors.toList());

        return schedules;
    }
}

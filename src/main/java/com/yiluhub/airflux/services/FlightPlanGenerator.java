package com.yiluhub.airflux.services;

import com.yiluhub.airflux.entities.*;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

public class FlightPlanGenerator {
    private FlightScheduleRepository flightScheduleRepository;
    private AircraftRepository aircraftRepository;
    private AirportRepository airportRepository;
    private ZonedDateTime baseTime = ZonedDateTime.parse("2018-04-13T00:00:00.000-03:00");

    public FlightPlanGenerator() {
        flightScheduleRepository = new FlightScheduleRepository();
        aircraftRepository = new AircraftRepository();
        airportRepository = new AirportRepository();
    }

    /**
     * Return all flights according to the flight schedule
     * @see FlightScheduleRepository
     * @return List of all available flights
     */
    public List<Flight> getAllFlights() {
        List<Flight> flights = new ArrayList<>();

        for (FlightSchedule schedule : flightScheduleRepository.getAll()) {
            Flight flight = convertFlightScheduleToFlight(schedule);
            flights.add(flight);
        }

        return flights;
    }

    private Flight convertFlightScheduleToFlight(FlightSchedule schedule) {
        Flight flight = new Flight();
        flight.setOrigin(schedule.getOrigin());
        flight.setDestination(schedule.getDestination());
        ZonedDateTime departure = baseTime.with(schedule.getTimeOfDeparture());
        flight.setDeparture(departure);
        ZonedDateTime arrival = departure
                .plusHours(schedule.getFlightTime().getHour())
                .plusMinutes(schedule.getFlightTime().getMinute());
        flight.setArrival(arrival);
        Airport airport = airportRepository.findById(schedule.getOrigin());
        Aircraft aircraft = aircraftRepository.aircraftAt(airport);
        flight.setEquipament(aircraft.getId());

        return flight;
    }
}

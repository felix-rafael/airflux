package com.yiluhub.airflux.services;

import com.yiluhub.airflux.entities.*;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

public class OperatingInstructions {
    private AircraftRepository aircraftRepository;
    private AirportRepository airportRepository;
    private FlightScheduleRepository flightScheduleRepository;
    private ZonedDateTime baseTime = ZonedDateTime.parse("2018-04-13T00:00:00.000-03:00");

    public OperatingInstructions() {
        this.aircraftRepository = new AircraftRepository();
        this.airportRepository = new AirportRepository();
        this.flightScheduleRepository = new FlightScheduleRepository();
    }

    /**
     * Generate all valid operating instructions for the given registration
     * @see AircraftRepository
     * @return list of operating instructions
     */
    public List<OperatingInstruction> generateOperatingInstructions(String registration) {
        List<OperatingInstruction> instructions = new ArrayList<>();
        Aircraft aircraft = aircraftRepository.findAircraftByRegistration(registration);
        if (aircraft == null) {
            return instructions;
        }
        Airport airport = airportRepository.findByCity(aircraft.getCity());
        if (airport == null) {
            return instructions;
        }
        List<FlightSchedule> schedules = flightScheduleRepository.getScheduleByOrigin(airport.getId());
        for (FlightSchedule schedule : schedules) {
            OperatingInstruction instruction = new OperatingInstruction();
            instruction.setOrigin(schedule.getOrigin());
            instruction.setDestination(schedule.getDestination());
            ZonedDateTime departure = baseTime.with(schedule.getTimeOfDeparture());
            instruction.setDeparture(departure);
            instructions.add(instruction);
        }

        return instructions;
    }
}

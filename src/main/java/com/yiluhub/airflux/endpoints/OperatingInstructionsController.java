package com.yiluhub.airflux.endpoints;

import com.yiluhub.airflux.entities.OperatingInstruction;
import com.yiluhub.airflux.services.OperatingInstructions;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class OperatingInstructionsController {
    @GetMapping(value = "/operationsplan", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<OperatingInstruction> getOperatingInstructions(@RequestParam("registration") String registration) {
        return new OperatingInstructions().generateOperatingInstructions(registration);
    }
}

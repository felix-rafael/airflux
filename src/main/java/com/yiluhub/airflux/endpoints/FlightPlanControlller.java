package com.yiluhub.airflux.endpoints;

import com.yiluhub.airflux.entities.Flight;
import com.yiluhub.airflux.services.FlightPlanGenerator;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.stream.Collectors;

@Controller
public class FlightPlanControlller {
    @GetMapping(value = "/flightplan", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Flight> allFlightPlans(@RequestParam(value = "airport", required = false) String airport) {
        List<Flight> flights = new FlightPlanGenerator().getAllFlights();

        if (airport == null || airport.isEmpty()) {
            return flights;
        }


        return flights.stream().filter(f -> f.getOrigin().equals(airport)).collect(Collectors.toList());
    }
}

package com.yiluhub.airflux;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AirfluxApplication {
	public static void main(String[] args) {
		SpringApplication.run(AirfluxApplication.class, args);
	}
}
